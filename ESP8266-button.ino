#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>

/////////////////////
// Pin Definitions //
/////////////////////
const int BUTTON_PIN = 2;
bool BUTTON_DEFAULT = HIGH;

/////////////////////////
// EEPROM Definitions //
/////////////////////////
#define VERSION "Version 001"
#define VERSION_SIZE 16
#define SSID_SIZE 64
#define PASS_SIZE 64
#define URL_SIZE 256
#define EEPROM_SIZE (VERSION_SIZE + SSID_SIZE + PASS_SIZE + URL_SIZE)

#define VERSION_OFFSET 0
#define SSID_OFFSET (VERSION_OFFSET + VERSION_SIZE)
#define PASS_OFFSET (SSID_OFFSET + SSID_SIZE)
#define URL_OFFSET (PASS_OFFSET + PASS_SIZE)



/////////////////////////
// Network Definitions //
/////////////////////////
const IPAddress AP_IP(192, 168, 1, 1);
const char* AP_SSID = "ESP8266_BUTTON";
String SSID_LIST;
DNSServer DNS_SERVER;
ESP8266WebServer WEB_SERVER(80);
boolean SETUP_MODE = true;


/////////////////////////
// BUTTON Definitions //
/////////////////////////
String BUTTON_TITLE = "ESP8266 Button";
String BUTTON_URL = "http://maker.ifttt.com/trigger/_event_/with/key/_key_";


//////////////////////
// Button Variables //
//////////////////////
int BUTTON_STATE;
int LAST_BUTTON_STATE = LOW;
long LAST_DEBOUNCE_TIME = 0;
long DEBOUNCE_DELAY = 100;
int BUTTON_COUNTER = 0;

void setup() {

  // Serial and EEPROM
  Serial.begin(115200);
  Serial.println("\n\nESP8266 Button");
  
  EEPROM.begin(EEPROM_SIZE);
  Serial.println("EEPROM Size: " + String(EEPROM_SIZE));
  delay(10);
  if(!checkEEPROM()){
    delay(5000);
    ESP.restart();
  }
  
  // Button
  pinMode(BUTTON_PIN, INPUT);

  WiFi.mode(WIFI_STA);  
  WiFi.disconnect();

  if(loadSavedConfig() && checkWiFiConnection()){
    SETUP_MODE = false;
  }else{
    Serial.println("\nSETUP_MODE\nScanning Networks:");
    
    delay(100);
    int n = WiFi.scanNetworks();
    delay(100);
    for (int i = 0; i < n; ++i) {
      Serial.println(" - " + String(WiFi.SSID(i)));
      SSID_LIST += "<option value=\"";
      SSID_LIST += WiFi.SSID(i);
      SSID_LIST += "\">";
      SSID_LIST += WiFi.SSID(i);
      SSID_LIST += "</option>";      
    }
    delay(100);
    Serial.println(String("AccessPoint SSID: ") + AP_SSID);
    WiFi.mode(WIFI_AP);
    WiFi.softAPConfig(AP_IP, AP_IP, IPAddress(255, 255, 255, 0));
    WiFi.softAP(AP_SSID);
    DNS_SERVER.start(53, "*", AP_IP);
  }

  startWebServer();
}

void loop() {

  // Handle WiFi Setup and Webserver for reset page
  if (SETUP_MODE) {
    DNS_SERVER.processNextRequest();
  }
  WEB_SERVER.handleClient();

  // Wait for button Presses
  if (debounce()) {
    BUTTON_COUNTER++;
    Serial.print("Button was pressed ");
    Serial.print(BUTTON_COUNTER);
    Serial.println(" times");    
    triggerButtonEvent();
  }
}

//////////////////////
// Button Functions //
//////////////////////
void triggerButtonEvent(){
  
  // Set some values for the JSON data depending on which event has been triggered
  String value_1 = BUTTON_TITLE;
  String value_2 = String(BUTTON_COUNTER -1);
  String value_3 = ipStr(WiFi.localIP());

  // Build JSON data string
  String data = "{\"value1\":\""+ value_1 +"\",\"value2\":\""+ value_2 +"\",\"value3\":\""+ value_3 + "\"}";

  HTTPClient http;
  http.begin(BUTTON_URL);
  http.addHeader("Content-Type", "application/json");
  String code = (String) http.POST(data);
  String payload = http.getString();
  Serial.println("HTTP-Code: " + code);
  Serial.println("Response: " + payload);
  http.end();
}

// Debounce Button Presses
boolean debounce() {
  boolean retVal = false;
  int reading = digitalRead(BUTTON_PIN);
  if (reading != LAST_BUTTON_STATE) {
    LAST_DEBOUNCE_TIME = millis();
  }
  if ((millis() - LAST_DEBOUNCE_TIME) > DEBOUNCE_DELAY) {
    if (reading != BUTTON_STATE) {
      BUTTON_STATE = reading;
      if (BUTTON_STATE != BUTTON_DEFAULT) {
        retVal = true;
      }
    }
  }
  LAST_BUTTON_STATE = reading;
  return retVal;
}

/////////////////////////////
// AP Setup Mode Functions //
/////////////////////////////

// Load Saved Configuration from EEPROM
boolean loadSavedConfig() {
  
  Serial.println("Load saved config.");
  
  if (EEPROM.read(SSID_OFFSET) != '\0' && EEPROM.read(PASS_OFFSET) != '\0' && EEPROM.read(URL_OFFSET) != '\0') {
    String ssid = readEepromString(SSID_OFFSET, SSID_SIZE);
    String pass = readEepromString(PASS_OFFSET, PASS_SIZE);
    BUTTON_URL = readEepromString(URL_OFFSET, URL_SIZE);    
    
    Serial.println("SSID: " + ssid);    
    Serial.println("URL: " + String(BUTTON_URL));

    WiFi.begin(ssid.c_str(), pass.c_str());
    return true;
  } else {
    Serial.println("No config found.");
    return false;
  }
}

// Boolean function to check for a WiFi Connection
boolean checkWiFiConnection() {
  int count = 0;
  Serial.println("Waiting to connect to the specified WiFi network");
  while ( count < 30 ) {
    if (WiFi.status() == WL_CONNECTED) {
      Serial.println();
      Serial.println("Connected!");
      return (true);
    }
    delay(500);
    Serial.print(".");
    count++;
  }
  Serial.println(" Timed out.");
  return false;
}

String makePage(String title, String contents) {
  String s = "<!DOCTYPE html><html><head>";
  s += "<meta name=\"viewport\" content=\"width=device-width,user-scalable=0\">";
  s += "<style>";
  // Simple Reset CSS
  s += "*,*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}html{font-size:100%;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}html,button,input,select,textarea{font-family:sans-serif}article,aside,details,figcaption,figure,footer,header,hgroup,main,nav,section,summary{display:block}body,form,fieldset,legend,input,select,textarea,button{margin:0}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}img{border:0}svg:not(:root){overflow:hidden}body{font-family:sans-serif;font-size:16px;font-size:1rem;line-height:22px;line-height:1.375rem;color:#585858;font-weight:400;background:#fff}p{margin:0 0 1em 0}a{color:#cd5c5c;background:transparent;text-decoration:underline}a:active,a:hover{outline:0;text-decoration:none}strong{font-weight:700}em{font-style:italic}";
  // Basic CSS Styles
  s += "body{font-family:sans-serif;font-size:16px;font-size:1rem;line-height:22px;line-height:1.375rem;color:#585858;font-weight:400;background:#fff}p{margin:0 0 1em 0}a{color:#cd5c5c;background:transparent;text-decoration:underline}a:active,a:hover{outline:0;text-decoration:none}strong{font-weight:700}em{font-style:italic}h1{font-size:32px;font-size:2rem;line-height:38px;line-height:2.375rem;margin-top:0.7em;margin-bottom:0.5em;color:#343434;font-weight:400}fieldset,legend{border:0;margin:0;padding:0}legend{font-size:18px;font-size:1.125rem;line-height:24px;line-height:1.5rem;font-weight:700}label,button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0}input{line-height:normal}.input{width:100%}input[type='text'],input[type='email'],input[type='tel'],input[type='date']{height:36px;padding:0 0.4em}input[type='checkbox'],input[type='radio']{box-sizing:border-box;padding:0}";
  // Custom CSS
  s += "header{width:100%;background-color: #2c3e50;top: 0;min-height:60px;margin-bottom:21px;font-size:15px;color: #fff}.content-body{padding:0 1em 0 1em}header p{font-size: 1.25rem;float: left;position: relative;z-index: 1000;line-height: normal; margin: 15px 0 0 10px}";
  s += "</style>";
  s += "<title>";
  s += title;
  s += "</title></head><body>";
  s += "<header><p>" + BUTTON_TITLE + "</p></header>";
  s += "<div class=\"content-body\">";
  s += contents;
  s += "</div>";
  s += "</body></html>";
  return s;
}

String urlDecode(String input) {
  String s = input;
  s.replace("%20", " ");
  s.replace("+", " ");
  s.replace("%21", "!");
  s.replace("%22", "\"");
  s.replace("%23", "#");
  s.replace("%24", "$");
  s.replace("%25", "%");
  s.replace("%26", "&");
  s.replace("%27", "\'");
  s.replace("%28", "(");
  s.replace("%29", ")");
  s.replace("%30", "*");
  s.replace("%31", "+");
  s.replace("%2C", ",");
  s.replace("%2E", ".");
  s.replace("%2F", "/");
  s.replace("%2C", ",");
  s.replace("%3A", ":");
  s.replace("%3A", ";");
  s.replace("%3C", "<");
  s.replace("%3D", "=");
  s.replace("%3E", ">");
  s.replace("%3F", "?");
  s.replace("%40", "@");
  s.replace("%5B", "[");
  s.replace("%5C", "\\");
  s.replace("%5D", "]");
  s.replace("%5E", "^");
  s.replace("%5F", "-");
  s.replace("%60", "`");
  return s;
}

String ipStr(IPAddress ip){
  return String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
}

String readEepromString(int _offset, int _size){
  String s = "";
  char c;
  for (int i = 0; i < _size; ++i) {
    c = char(EEPROM.read(_offset + i));
    if(c == 0) break;
    s += c;
  }
  return s;
}

void writeEepromString(String _s, int _offset, int _size, bool commit = true){
  _s = _s.substring(0,_size-1); // cut String
  
  for (int i = 0; i < _s.length(); ++i) {
    EEPROM.write(_offset + i, _s[i]);
  }
  EEPROM.write(_offset + _s.length(), '\0');

  if(commit){
    EEPROM.commit();
  }
}

void clearEeprom(int _offset, int _size, bool commit = true){
  for (int i = 0; i < _size; ++i) {
    EEPROM.write(_offset + i, '\0');
  }
  if(commit){
    EEPROM.commit();
  }
}

bool checkEEPROM(){
  Serial.print("Checking EEPROM: ");
  String v = readEepromString(VERSION_OFFSET, VERSION_SIZE);

  if(!v.equals(VERSION)){
    Serial.println("Incorrect EEPROM Version - Cleanup");
    
    writeEepromString(VERSION, VERSION_OFFSET, VERSION_SIZE);
    delay(1000);

    // Testing
    v = readEepromString(VERSION_OFFSET, VERSION_SIZE);    
    if(!v.equals(VERSION)){
      Serial.println("Writing Failed!");
      return false;
    }
  }else{
    Serial.println("ok");
  }
  return true;
}

// Start the web server and build out pages
void startWebServer() {
  if (SETUP_MODE) {
    
    String ipStr = String(AP_IP[0]) + '.' + String(AP_IP[1]) + '.' + String(AP_IP[2]) + '.' + String(AP_IP[3]);
    Serial.println("Starting Web Server at IP address: " + ipStr);
    
    // Settings Page
    WEB_SERVER.on("/settings", []() {
      String s = "<h2>Wi-Fi Settings</h2><p>Please select the SSID of the network you wish to connect to and then enter the password and submit.</p>";
      s += "<form method=\"get\" action=\"setup\">";
      s += "<table>";
      s += "<tr><td>SSID:</td><td><select class=\"input\" name=\"ssid\">" + SSID_LIST + "</select></td></tr>";
      s += "<tr><td>Password:</td><td><input class=\"input\" name=\"pass\" length=63 type=\"text\"></td></tr>";
      s += "<tr><td>URL:</td><td><input class=\"input\" name=\"url\" length=255 type=\"text\" value=\"" +BUTTON_URL+ "\"></td></tr>";
      s += "</table>";
      s += "<br><br>";
      s += "<input type=\"submit\">";
      s += "<br><br>";
      s += "<i>Wichtige Hinweise:<br />- SSID, Passwort: max 64 Zeichen<br />- URL: max 256 Zeichen<br /></i>";
      s += "</form>";
      WEB_SERVER.send(200, "text/html", makePage("Wi-Fi Settings", s));
    });
    // setap Form Post
    WEB_SERVER.on("/setup", []() {

      String ssid = urlDecode(WEB_SERVER.arg("ssid"));      
      String pass = urlDecode(WEB_SERVER.arg("pass"));      
      String url = urlDecode(WEB_SERVER.arg("url"));

      Serial.println("SSID: " + ssid);
      Serial.println("Password: " + pass);
      Serial.println("URL: " + url);
      
      Serial.println("Writing Setup to EEPROM...");
      
      writeEepromString(ssid, SSID_OFFSET, SSID_SIZE, false);
      writeEepromString(pass, PASS_OFFSET, PASS_SIZE, false);
      writeEepromString( url,  URL_OFFSET,  URL_SIZE, true);
      
      String s = "<h1>WiFi Setup complete.</h1><p>The button will be connected automatically to \"" + ssid + "\" after the restart.";
      WEB_SERVER.send(200, "text/html", makePage("Wi-Fi Settings", s));

      Serial.println("Write EEPROM done! - Restarting...");
      delay(5000);
      ESP.restart();
    });
    // Show the configuration page if no path is specified
    WEB_SERVER.onNotFound([]() {
      String s = "<h1>WiFi Configuration Mode</h1><p><a href=\"/settings\">Wi-Fi Settings</a></p>";
      WEB_SERVER.send(200, "text/html", makePage("Access Point mode", s));
    });
  } else {
    
    Serial.println("Starting Web Server at " + ipStr(WiFi.localIP()));
    
    WEB_SERVER.on("/", []() {
      String s = "<h1>IoT Button Status</h1>";
      s += "<h3>Network Details</h3>";
      s += "<table>";
      s += "<tr><td>Connected to:</td><td>" + String(WiFi.SSID()) + "</td></tr>";
      s += "<tr><td>IP Address:</td><td>" + ipStr(WiFi.localIP()) + "</td></tr>";
      s += "</table>";
      s += "<h3>Button Details</h3>";
      s += "<table>";
      s += "<tr><td>URL:</td><td>" + String(BUTTON_URL) + "</td></tr>";
      s += "<tr><td>Button Presses:</td><td>" + String(BUTTON_COUNTER) + "</td></tr>";
      s += "</table>";
      s += "<h3>Options</h3>";
      s += "<p><a href=\"/changeURL\">Change URL</a><br /><a href=\"/reset\">Factory Reset</a></p>";
      WEB_SERVER.send(200, "text/html", makePage("Station mode", s));
    });
    WEB_SERVER.on("/changeURL", []() {
      String s = "<h2>Change URL</h2>";
      s += "<form method=\"get\" action=\"setURL\">";
      s += "<table>";
      s += "<tr><td>SSID:</td><td>" + String(WiFi.SSID()) + "</td></tr>";
      s += "<tr><td>URL:</td><td><input class=\"input\" name=\"url\" length=255 type=\"text\" value=\"" +BUTTON_URL+ "\"></td></tr>";
      s += "</table>";
      s += "<br><br>";
      s += "<input type=\"submit\">";
      s += "<br><br>";
      s += "<i>Wichtiger Hinweis:<br />- URL: max 256 Zeichen<br /></i>";
      s += "</form>";
      WEB_SERVER.send(200, "text/html", makePage("Change URL", s));
    });
    // setap Form Post
    WEB_SERVER.on("/setURL", []() {      
      BUTTON_URL = urlDecode(WEB_SERVER.arg("url"));

      Serial.println("setURL: " + BUTTON_URL);  
      Serial.println("Writing Setup to EEPROM...");
      
      writeEepromString(BUTTON_URL, URL_OFFSET, URL_SIZE, true);

      WEB_SERVER.sendHeader("Location", String("/"), true);
      WEB_SERVER.send ( 302, "text/plain", "");
    });
    WEB_SERVER.on("/reset", []() {
      clearEeprom(0,EEPROM_SIZE);
      String s = "<h1>Settings was reset.</h1><p>Please reconnect to SSID \"" + String(AP_SSID) + "\".</p>";
      WEB_SERVER.send(200, "text/html", makePage("Reset Wi-Fi Settings", s));
      delay(5000);
      ESP.restart();
    });
  }
  WEB_SERVER.begin();
}
